<?php

namespace app\controllers;

use app\controllers\actions\CallbackAction;
use app\controllers\actions\FeedbackAction;
use yii\easyii\models\Tag;
use yii\easyii\modules\article\api\Article;
use yii\easyii\modules\catalog\api\Catalog;
use yii\easyii\modules\entity\api\Entity;
use yii\easyii\modules\page\api\Page;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' =>  'yii\web\ErrorAction',
            'feedback' =>FeedbackAction::class,
            'callback' =>CallbackAction::class,
        ];
    }

    public function actionIndex()
    {
        $page = Page::get('index');
        if(!$page){
            throw new NotFoundHttpException('Item not found.');
        }

        $features = Entity::cat('features');
        $address = Entity::cat('address');

        $kley = Catalog::cat('klei');
        $syrie = Catalog::cat('syre-dlya-kleya');
        $blog = Article::cat('blog');
        $tags = Tag::find()->orderBy('frequency DESC')->limit(10)->asArray()->all();

        $tk = Entity::cat('transportnye-kompanii');

        return $this->render('index',[
            'page'=>$page,
            'features'=>$features,
            'address'=>$address,

            'kley'=>$kley,
            'kley_items'=>$kley->getItems([
                'pagination' => ['pageSize' => 4],
            ]),

            'syrie'=>$syrie,
            'syrie_items'=>$syrie->getItems([
                'pagination' => ['pageSize' => 4],
            ]),

            'blog'=>$blog,
            'tags'=>$tags,

            'tk' => $tk->getItems(),
        ]);
    }
}