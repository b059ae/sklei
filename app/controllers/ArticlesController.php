<?php

namespace app\controllers;

use yii\easyii\models\Tag;
use yii\easyii\modules\article\api\Article;

class ArticlesController extends \yii\web\Controller
{
    /*public function actionIndex()
    {
        return $this->render('index');
    }*/

    public function actionCat($slug, $tag = null)
    {
        $cat = Article::cat($slug);
        if (!$cat) {
            throw new \yii\web\NotFoundHttpException('Article category not found.');
        }
        $tags = Tag::find()->orderBy('frequency DESC')->limit(10)->asArray()->all();

        return $this->render('cat', [
            'cat' => $cat,
            'items' => $cat->getItems(['tags' => $tag, 'pagination' => ['pageSize' => 4]]),
            'tags' => $tags,
        ]);
    }

    public function actionView($category,$slug)
    {
        $cat = Article::cat($category);
        if(!$cat){
            throw new \yii\web\NotFoundHttpException('Article category not found.');
        }
        $article = Article::get($slug);
        if(!$article){
            throw new \yii\web\NotFoundHttpException('Article not found.');
        }

        return $this->render('view', [
            'article' => $article
        ]);
    }

}
