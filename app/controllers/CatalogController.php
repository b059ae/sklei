<?php

namespace app\controllers;

use Yii;
use yii\easyii\modules\catalog\api\Catalog;
use \app\models\Catalog as Ctlg;
use yii\easyii\modules\entity\api\Entity;
use yii\easyii\modules\page\api\Page;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;

class CatalogController extends \yii\web\Controller
{
    /**
     * Список категорий
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        /*
        $filterForm = new \app\models\FilterForm();
        $filters = [];
        if($filterForm->load(Yii::$app->request->get()) && $filterForm->validate()) {
            $filters = $filterForm->parse();
        }

        $catalog = [];

        //Клей
        $cat = Catalog::cat('klei');
        $properties = $cat->fieldOptions('properties');
        $filterForm->setPropertiesOptions($properties);
        $filterForm->setColorOptions($cat->fieldOptions('color'));
        $filterForm->setPackageOptions($cat->fieldOptions('package'));
        foreach ($properties as $property){
            if (isset($filters['properties']) && !in_array($property, $filters['properties'])){
                continue;
            }
            $itemsFilters = $filters;
            $itemsFilters['properties'] = $property;
            $catalog[] = new Ctlg([
                'title' => 'Клей '.$property,
                'cat' => $cat,
                'items' => $cat->getItems([
                    'filters' => $itemsFilters,
                ])
            ]);
        }
        //Сырье для клея
        $cat = Catalog::cat('syre-dlya-kleya');
        $properties = $cat->fieldOptions('properties');
        $filterForm->setPropertiesOptions($properties);
        $filterForm->setColorOptions($cat->fieldOptions('color'));
        $filterForm->setPackageOptions($cat->fieldOptions('package'));
        foreach ($properties as $property){
            if (isset($filters['properties']) && !in_array($property, $filters['properties'])){
                continue;
            }
            $itemsFilters = $filters;
            $itemsFilters['properties'] = $property;
            $catalog[] = new Ctlg([
                'title' => $property,
                'cat' => $cat,
                'items' => $cat->getItems([
                    'filters' => $itemsFilters,
                ])
            ]);
        }

        //Оборудование
        $cat = Catalog::cat('oborudovanie');
        $properties = $cat->fieldOptions('properties');
        $filterForm->setPropertiesOptions($properties);
        foreach ($properties as $property){
            if (isset($filters['properties']) && !in_array($property, $filters['properties'])){
                continue;
            }
            $itemsFilters = $filters;
            $itemsFilters['properties'] = $property;
            $catalog[] = new Ctlg([
                'title' => $property,
                'cat' => $cat,
                'items' => $cat->getItems([
                    'filters' => $itemsFilters,
                ])
            ]);
        }

        //Фурнитура
        $cat = Catalog::cat('furnitura');
        $properties = $cat->fieldOptions('properties');
        $filterForm->setPropertiesOptions($properties);
        foreach ($properties as $property){
            if (isset($filters['properties']) && !in_array($property, $filters['properties'])){
                continue;
            }
            $itemsFilters = $filters;
            $itemsFilters['properties'] = $property;
            $catalog[] = new Ctlg([
                'title' => $property,
                'cat' => $cat,
                'items' => $cat->getItems([
                    'filters' => $itemsFilters,
                ])
            ]);
        }


        */
        $catalogs = Catalog::cats();
        /*foreach($catalogs as $ctlg){
            //echo '<strong>' . $ctlg->title . '</strong><br/>';
            //echo $ctlg->description . '<br/><br/>';
            //print_r(get_object_vars($ctlg));
            //print_r(get_class_methods($ctlg));break;
        }*/
        $page = Page::get('catalog');
        if(!$page){
            throw new NotFoundHttpException('Item not found.');
        }
        return $this->render('index',[
            'catalogs'=>$catalogs,
            'page'=>$page,
            //'catalog'=>$catalog,
            //'filterForm'=>$filterForm,
        ]);
    }

    /**
     * Список товаров в категории
     * @param string $slug Категория
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCat($slug)
    {
        $catalog = [];
        $cat = Catalog::cat($slug);
        if(!$cat){
            throw new NotFoundHttpException('Category not found.');
        }
        $properties = $cat->fieldOptions('properties');
        foreach ($properties as $property){
            $catalog[] = new Ctlg([
                'title' => $property,
                'cat' => $cat,
                'items' => $cat->getItems([
                    'pagination' => false,
                    'filters' => [
                        'properties' => $property,
                    ],
                ])
            ]);
        }
        return $this->render('cat', [
            'cat' => $cat,
            'catalog'=>$catalog,
        ]);
    }

    /*protected function getFilterForm($slug){
        switch ($slug){
            case 'syre-dlya-kleya':
                return new \app\models\SyryeFilterForm();
            case 'klei':
                return new \app\models\KleyFilterForm();
            default:
                throw new NotFoundHttpException('Category not found.'); 
        }
    }*/

    /*public function actionSearch($text)
    {
        $text = filter_var($text, FILTER_SANITIZE_STRING);

        return $this->render('search', [
            'text' => $text,
            'items' => Catalog::items([
                'where' => ['or', ['like', 'title', $text], ['like', 'description', $text]],
            ])
        ]);
    }*/

    /**
     * Просмотр полной информации о товаре
     * @param string $category Категория
     * @param string $slug Товар
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($category, $slug)
    {
        $cat = Catalog::cat($category);
        if(!$cat){
            throw new NotFoundHttpException('Category not found.');
        }
        $item = Catalog::get($slug);
        if(!$item){
            throw new NotFoundHttpException('Item not found.');
        }
        
        $address = Entity::cat('address');

        /*$filterForm = new \app\models\FilterForm();
        $filterForm->setAllOptions();*/

        return $this->render($category.'/view', [
            'item' => $item,
            'cat' => $cat,
            'address'=>$address,
//            'filterForm' => $filterForm
//            'addToCartForm' => new \app\models\AddToCartForm()
        ]);
    }
}
