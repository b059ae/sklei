<?php
namespace app\controllers\actions;
use Yii;
use yii\web\BadRequestHttpException;

/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 30.07.2017
 * Time: 17:53
 */

class FeedbackAction extends \yii\web\ErrorAction
{
    public function run()
    {
        $model = new \app\models\FeedbackForm();

        $request = Yii::$app->request;

        if ($model->load($request->post()) && $model->save()) {
            return ;
        } else {
            throw new BadRequestHttpException('При отправке запроса произошла ошибка');
        }
    }
}
