<?php
namespace app\controllers\actions;
use Yii;
use yii\web\BadRequestHttpException;

/**
 * User: Alexander Popov <b059ae@gmail.com>
 * Date: 30.07.2017
 * Time: 17:53
 */

class CallbackAction extends \yii\web\ErrorAction
{
    /**
     * @return string result content
     */
    public function run()
    {
        $model = new \app\models\CallbackForm();

        $request = Yii::$app->request;

        if ($model->load($request->post()) && $model->save()) {
            return ;
        } else {
            throw new BadRequestHttpException('При отправке запроса произошла ошибка');
        }
    }
}
