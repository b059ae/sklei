<?php
/** @var $this \yii\web\View */
?>
<!-- Float Button -->
<div class="float_btn hidden-xs">
    <a class="float_btn_link" data-toggle="modal" data-target=".float-button-modal" onclick="metrikaReachGoal('float-button-modal')">
        <div class="float_btn_block">
            <div class="float_btn_a"></div>
            <div class="float_btn_b"></div>
        </div>
    </a>
</div>
<!-- End Float Button -->