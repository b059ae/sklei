<?php
/** @var $this \yii\web\View */
/** @var $asset \yii\web\AssetBundle */
use app\helpers\Html;
use yii\easyii\models\Setting;
use yii\helpers\ArrayHelper;
use yii\widgets\Menu;

?>
<footer>
    <div class="container">
    <div class="row">
        <div class="footer-nav col-sm-12 col-xs-12">
            <div class="row">
            <div class="footer-logo col-md-3 col-xs-12">
                <img src="<?= $asset->baseUrl ?>/img/logo_footer.png" alt="С-Клей"/>
            </div>
            <div class="footer-phone col-md-3 col-xs-12">
                <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span><?= Html::phone(Setting::get('phone')); ?>
            </div>
            <div class="footer-address col-md-3 col-xs-12">
                <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><?= Html::mailto(Setting::get('email')) ?>
            </div>
            <?php /*= Menu::widget([
                'items' => yii\easyii\modules\menu\api\Menu::items('main'),
                'options' => [
                    'class' => 'nav nav-pills col-md-6 col-sm-12 col-xs-12',
                ]
            ]);*/ ?>
            <div class="footer-copyright col-md-3 col-xs-12">
                &copy; <?=date('Y')?>. Все права защищены.
            </div>
        </div>
        </div>
    </div>
    </div>

</footer>