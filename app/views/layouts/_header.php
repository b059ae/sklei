<?php
/** @var $this \yii\web\View */
/** @var $asset \yii\web\AssetBundle */
use yii\easyii\models\Setting;
use yii\helpers\Url;
use yii\widgets\Menu;

?>

    <div class="logo">
        <div class="container">
            <div class="navbar-brand col-md-3 col-sm-4">
                <a href="<?=Yii::$app->homeUrl?>">
                    <img src="<?= $asset->baseUrl ?>/img/logo.png" alt="С-Клей" class="hidden-xs"/>
                    <img src="<?= $asset->baseUrl ?>/img/logo-xs.png" alt="С-Клей" class="visible-xs"/>
                </a>
            </div>
            <div class="phone col-md-5 col-md-push-4 col-sm-8 col-xs-7 text-right">
                <?= \app\helpers\Html::phone(Setting::get('phone')); ?>
                <div class="modal_btn">
                    <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span><a data-toggle="modal" data-target="#callback-modal" onclick="metrikaReachGoal('callback-modal')">Заказать звонок</a>

                    <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="<?=Url::to(['/kontaktnaya-informaciya'])?>">Обратная связь</a>
                </div>
            </div>
        </div>
    </div>
    <nav id="nav" class="navbar navbar-default" style="background-color: transparent; border: 0;">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <?= Menu::widget([
                'items' => yii\easyii\modules\menu\api\Menu::items('main')/*ArrayHelper::merge(
                    yii\easyii\modules\menu\api\Menu::items('glavnoe-menu'),
                    \yii\easyii\modules\page\api\Page::menu()
                )*/,
                'options' => [
                    'id'=>"navbar",
                    'class' => 'nav nav-justified navbar-collapse collapse',
                ]
            ]); ?>
        </div>
    </nav>
<?php
//Приклеиваем навбар к верху экрана при скролинге
$script = <<< JS
    $('#nav').affix({
        offset: {
            top: 50
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter41462809 = new Ya.Metrika({
                    id:41462809,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<!-- /Yandex.Metrika counter -->

<!-- Цели -->
<script>
    function metrikaReachGoal(name, params){
        if (typeof yaCounter41462809 != "undefined") {
            if (typeof params == "undefined"){
                params = {ab_test: "<?=Yii::$app->ABTest->getValue()?>"};
            }
            yaCounter41462809.reachGoal(name, params);
        }
        /* ga('send', 'event', 'metrika', name); */
    }
    <!--  Применение событий -->
    <?php /*
    window.onload = function(){
        <?= Yii::$app->session->getFlash('metrika');?>
    }
    */ ?>
</script>
<!-- /Цели -->