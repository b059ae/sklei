<?php
/** @var $this \yii\web\View */
?>
<div id="callback-modal" class="modal fade callback-modal" tabindex="-1" role="dialog" aria-labelledby="callbackModal" aria-hidden="true">
    <div class="modal-dialog modal-lg flex">
        <div class="modal-content callback">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="callback-h"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> Заказать звонок</div>
            <hr>
            <p>Наши специалисты оперативно свяжутся с Вами для подтверждения заказа, проконсультируют по видам и характеристикам продукции,
                а также помогут подобрать товар.</p>
            <?= \app\widgets\CallbackForm::widget() ?>
        </div>
    </div>
</div>

<!--Форма заказа звонка с первого слайда-->
<div id="main-button-modal" class="modal fade main-button-modal" tabindex="-1" role="dialog" aria-labelledby="callbackModal" aria-hidden="true">
    <div class="modal-dialog modal-lg flex">
        <div class="modal-content callback">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="callback-h"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> Оформить заказ</div>
            <hr>
            <p>Наши специалисты оперативно свяжутся с Вами для подтверждения заказа, проконсультируют по видам и характеристикам продукции,
                а также помогут подобрать товар.</p>
            <?= \app\widgets\CallbackForm::widget([
                'button' => 'Заказать',
                'metrikaGoal' => 'main-button-callback',
            ]) ?>
        </div>
    </div>
</div>

<!--Форма заказа звонка с плавающей кнопки-->
<div id="float-button-modal" class="modal fade float-button-modal" tabindex="-1" role="dialog" aria-labelledby="callbackModal" aria-hidden="true">
    <div class="modal-dialog modal-lg flex">
        <div class="modal-content callback">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="callback-h"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> Обратный звонок</div>
            <hr>
            <p>Наши специалисты свяжутся с Вами в течение 1 минуты.</p>
            <?= \app\widgets\CallbackForm::widget([
                'button' => 'Жду звонка',
                'metrikaGoal' => 'float-button-callback',
            ]) ?>
        </div>
    </div>
</div>