<?php
/** @var $this \yii\web\View */
use yii\helpers\Html;

$asset = \app\assets\AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="shortcut icon" href="<?= $asset->baseUrl ?>/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= $asset->baseUrl ?>/favicon.ico" type="image/x-icon">
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <?= $this->render('_header', ['asset' => $asset]); ?>
    <?= $this->render('_callback_modal'); ?>
    <?= $this->render('/catalog/_order_modal'); ?>
    <?= $content ?>
    <?= $this->render('_callback_float'); ?>
    <?= $this->render('_footer', ['asset' => $asset]); ?>
    <?php
    // Модальные окна. Рендерим в одном месте
    if (isset(Yii::$app->view->params['modals']) && is_array(Yii::$app->view->params['modals'])) {
        foreach (Yii::$app->view->params['modals'] as $modal) {
            echo $modal;
        }
    }
    ?>
    <?php $this->endBody() ?>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
    </body>
    </html>
<?php $this->endPage() ?>