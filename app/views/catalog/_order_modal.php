<?php
/** @var $this \yii\web\View */
?>
<div id="order-modal" class="modal fade order-modal" tabindex="-1" role="dialog" aria-labelledby="orderModal" aria-hidden="true">
    <div class="modal-dialog modal-lg flex">
        <div class="modal-content callback">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="callback-h"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> Оформить заказ</div>
            <hr>
            <p>Наши специалисты оперативно свяжутся с Вами для подтверждения заказа, проконсультируют по видам и характеристикам продукции,
                а также помогут подобрать товар.</p>
            <?= \app\widgets\CallbackForm::widget(['button'=>'Оформить заказ']) ?>
        </div>
    </div>
</div>