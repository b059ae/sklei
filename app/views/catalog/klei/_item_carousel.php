<?php
/** @var $item \yii\easyii\modules\catalog\api\ItemObject */
/** @var $cat \yii\easyii\modules\catalog\api\CategoryObject */
use app\helpers\Html;
use yii\helpers\Url;

?>
<div class="col-item">
    <div class="info col-md-10 col-md-offset-2 col-xs-12">
        <div class="col-md-6 col-xs-12">
            <a href="<?= Url::to(['catalog/view', 'category' => $cat->slug, 'slug' => $item->slug]) ?>">
                <?= Html::img($item->thumb(350, 380)) ?>
            </a>
        </div>
        <div class="col-md-6 col-xs-12">
            <a href="<?= Url::to(['catalog/view', 'category' => $cat->slug, 'slug' => $item->slug]) ?>">
                <h3 class="clearfix"><?= $item->title ?>
                    <span class="price">
                    <?php if (!empty($item->getPrice())): ?>
                        от <?=$item->getPrice()?> руб.
                    <?php else:?>
                        цена по запросу
                    <?php endif;?>
                    </span>
                </h3>
                <p>
                    <?= $item->shortdescription ?>
                </p>
            </a>
            <div class="controls">
                <a href="<?= \yii\helpers\Url::to(['/catalog']) ?>" class="btn btn-catalog">Каталог
                    клея</a>
                <a class="left btn" href="#klei-carousel"
                   data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a class="right btn"
                   href="#klei-carousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </div>
        <div class="clearfix">
        </div>
    </div>
</div>