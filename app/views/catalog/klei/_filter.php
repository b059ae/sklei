<?php
/** @var  $items \yii\easyii\modules\catalog\api\ItemObject[] */
/** @var  $cat \yii\easyii\modules\catalog\api\CategoryObject */
/** @var  $filterForm \app\models\KleyFilterForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<p>Выберите параметры</p>
<div class="well well-sm">
<?php $form = ActiveForm::begin(['method' => 'get', 'action' => Url::to(['/catalog/cat', 'slug' => $cat->slug])]); ?>
<?= $form->field($filterForm, 'properties')->dropDownList($cat->fieldOptions('properties', 'Не выбрано'),['onchange'=>'this.form.submit()']) ?>
<?= $form->field($filterForm, 'color')->dropDownList($cat->fieldOptions('color', 'Не выбрано'),['onchange'=>'this.form.submit()']) ?>
<?= $form->field($filterForm, 'package')->dropDownList($cat->fieldOptions('package', 'Не выбрано'),['onchange'=>'this.form.submit()']) ?>
<?php ActiveForm::end(); ?>
</div>