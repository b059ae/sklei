<?php

/** @var $item \yii\easyii\modules\catalog\api\ItemObject */
use yii\easyii\modules\feedback\api\Feedback;

?>
<?php if (!empty($item->data->color)) : ?>
    <h3>Цвет:</h3>
    <ul>
        <?php foreach ($item->data->color as $color): ?>
            <li><?= $color ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
<?php if (!empty($item->data->package)) : ?>
    <h3>Упаковка:</h3>
    <ul>
        <?php foreach ($item->data->package as $package): ?>
            <li><?= $package ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
<?php if (!empty($item->data->properties)) : ?>
    <h3>Свойства:</h3>
    <ul>
        <?php foreach ($item->data->properties as $properties): ?>
            <li><?= $properties ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
