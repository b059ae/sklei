<?php
/** @var $item \yii\easyii\modules\catalog\api\ItemObject */

/** @var $cat \yii\easyii\modules\catalog\api\CategoryObject */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="catalog_item">
    <div class="col catalog_image">
        <a href="<?= Url::to(['catalog/view', 'category' => $item->cat->slug, 'slug' => $item->slug]) ?>">
            <?= Html::img($item->thumb(95, 95), ['class' => 'img-responsive']) ?>
        </a>
    </div>
    <div class="col catalog_desc">
        <h3>
            <?= Html::a($item->title, ['catalog/view', 'category' => $item->cat->slug, 'slug' => $item->slug]) ?>
        </h3>
        <p>
            <?php if (!empty($item->data->pressure)) : ?>
                <span class="text-muted">Давление:</span> <?= implode(', ', $item->data->pressure) ?>
            <?php endif; ?>
        </p>
        <p>
            <?php if (!empty($item->data->weight)) : ?>
                <span class="text-muted">Вес:</span> <?= implode(', ', $item->data->weight) ?>
            <?php endif; ?>
        </p>
    </div>
    <div class="col catalog_price">
        <div class="price">
            <?php if (!empty($item->getPrice())): ?>
                от <?= $item->getPrice() ?> руб.
            <?php else: ?>
                цена по запросу
            <?php endif; ?>
        </div>
        <?= Html::a('Подробнее', ['catalog/view', 'category' => $item->cat->slug, 'slug' => $item->slug], ['class' => 'btn']) ?>
    </div>
</div>