<?php

/** @var $item \yii\easyii\modules\catalog\api\ItemObject */
use yii\easyii\modules\feedback\api\Feedback;

?>

    <p>
        <?php if (!empty($item->data->pressure)) : ?>
            <span class="text-muted">Давление:</span> <?= implode(', ', $item->data->pressure) ?>
        <?php endif; ?>
    </p>
    <p>
        <?php if (!empty($item->data->weight)) : ?>
            <span class="text-muted">Вес:</span> <?= implode(', ', $item->data->weight) ?>
        <?php endif; ?>
    </p>
<?php /*
    <p>
        <span class="text-muted">Вид сырья:</span>
        <ul>
            <?php foreach ($item->data->properties as $properties): ?>
                <li><?= $properties ?></li>
            <?php endforeach; ?>
        </ul>
    </p> */ ?>
<?= $item->description ?>
