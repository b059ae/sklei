<?php
/** @var $item \yii\easyii\modules\catalog\api\ItemObject */
/** @var  $cat \yii\easyii\modules\catalog\api\CategoryObject */
/** @var $address \yii\easyii\modules\entity\api\CategoryObject */

use yii\bootstrap\Tabs;
use yii\easyii\modules\catalog\api\Catalog;
use yii\helpers\Html;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $item->seo('description')
]);
$this->title = $item->seo('title', $item->model->title);
$this->params['breadcrumbs'][] = ['label' => 'Shop', 'url' => ['shop/index']];
$this->params['breadcrumbs'][] = ['label' => $item->cat->title, 'url' => ['shop/cat', 'slug' => $item->cat->slug]];
$this->params['breadcrumbs'][] = $item->model->title;
?>

<section class="page">
    <div class="container">
        <div class="col-md-3">
            <?php /* <div class="pr_card_menu col-md-12 col-sm-6">
                <?=$this->render('//catalog/_filter',['cat'=>$cat, 'filterForm' => $filterForm]);?>
            </div> */ ?>
            <?=$this->render('//catalog/_cat_info');?>
        </div>
        <div class="col-md-9">
            <div class="header col-md-12 col-sm-12">
                <h1><?= $item->seo('h1', $item->title) ?></h1>
            </div>
            <div class="product col-md-12">
                <div class="row">
                    <div class="col-md-5">
                        <?= Html::img($item->thumb(350),['class'=>'img-responsive']) ?>
                        <?php if(count($item->photos)) : ?>
                            <br/><br/>
                            <div>
                                <?php foreach($item->photos as $photo) : ?>
                                    <?= $photo->box(null, 100) ?>
                                <?php endforeach;?>
                                <?php Catalog::plugin() ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-7">
                        <div class="col-lg-12">
                            <h3 class="clearfix">
                                <?php if (!empty($item->getPrice())): ?>
                                    от <?=$item->getPrice()?> руб.
                                <?php else:?>
                                    цена по запросу
                                <?php endif;?>
                            </h3>
                            <p><span class="blue">В наличии</span></p>
                            <button class="btn" data-toggle="modal" data-target="#order-modal" onclick="metrikaReachGoal('callback-modal')">Оформить <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"> </button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <p class="head">Доставка - 1 день</p>
                            <div class="col-md-1 col-xs-1">
                                <span class="glyphicon glyphicon-road" aria-hidden="true"></span>
                            </div>
                            <div class="col-md-11">
                                <p class="desc">
                                    Доставка до ТК или склада в Москве и МО бесплатная, остальные регионы - нужно уточнять
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <p class="head">Самовывоз - сегодня</p>
                            <div class="col-md-1 col-xs-1"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span></div>
                            <div class="col-md-11">
                                <p class="desc"><?= $address->getItems()[0]->{'address-production'} ?></p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="product_details col-lg-9">
            <?= Tabs::widget([
                'items' => [
                    [
                        'label' => 'Описание',
                        'content' => $this->render('_view_description', ['item' => $item]),
                        'active' => true
                    ],
                ]
            ]); ?>

            <?php if (isset($item->data->extra)):?>
                <?=$item->data->extra?>
            <?php endif; ?>
        </div><!--/container-->
</section>