<?php
/** @var  $cat \yii\easyii\modules\catalog\api\CategoryObject */

use app\helpers\Html;

/** @var  $catalog \app\models\Catalog[] */


$this->title = $cat->seo('title', $cat->model->title);
$this->params['breadcrumbs'][] = ['label' => 'Shop', 'url' => ['shop/index']];
$this->params['breadcrumbs'][] = $cat->model->title;
$catalogCount = count($catalog);
?>
<section class="page">
    <div class="container">
        <div class="col-md-9 col-md-push-3 col-sm-12">
            <div class="header">
                <h1><?= $cat->seo('h1', $cat->title) ?></h1>
            </div>
            <?php foreach ($catalog as $ctlg): ?>
                <?php if (count($ctlg->items)) : ?>
                    <div class="catalog">
                        <?php if ($catalogCount > 1) : ?>
                            <!-- Подкатегории -->
                            <div class="header">
                                <h2><?= Html::ucfirst($ctlg->title) ?></h2>
                            </div>
                        <?php endif; ?>
                        <?php foreach ($ctlg->items as $item) : ?>
                            <?= $this->render($cat->slug . '/_item', ['item' => $item]); ?>
                        <?php endforeach; ?>
                    </div>
                <?php else : ?>
                    <p>Category is empty</p>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <div class="col-md-3 col-md-pull-9 col-sm-12">
            <?= $this->render('//catalog/_cat_info'); ?>
        </div>
    </div><!--/container-->
</section>