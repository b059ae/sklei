<?php
/** @var  $catalog \app\models\Catalog[] */
/** @var $page \yii\easyii\modules\page\api\PageObject */
/** @var  $filterForm \app\models\FilterForm */

/** @var  $catalogs \yii\easyii\modules\catalog\api\CategoryObject[] */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);
$this->title = $page->seo('title', $page->title);
$this->params['breadcrumbs'][] = $page->title;
?>
<section class="page">
    <div class="container">
        <?php /*
        <div class="col-md-3 col-sm-12">
            <div class="pr_card_menu col-md-12 col-sm-6">
                <?= $this->render('_filter', ['filterForm' => $filterForm]); ?>
            </div>
            <?= $this->render('//catalog/_cat_info'); ?>
        </div>

        <div class="catalog col-md-9 col-sm-12">
            <?php foreach ($catalog as $ctlg): ?>
                <?php if (count($ctlg->items)) : ?>
                    <div class="row">
                        <div class="header col-md-12 col-sm-12">
                            <h2><?= $ctlg->cat->seo('h1', $ctlg->title) ?></h2>
                                <br/>
                                <?php foreach ($ctlg->items as $item) : ?>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <div class="catalog_block">
                                            <?= $this->render($ctlg->cat->slug.'/_item', ['item' => $item, 'cat' => $ctlg->cat]) ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <div class="pagination">
                                <?= $ctlg->cat->getPages() ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>

        */ ?>
        <div class="header col-md-12 col-sm-12 col-xs-12">
            <h1><?= $page->seo('h1', 'Каталог') ?></h1>
        </div>
        <div class="catalog col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <?php foreach ($catalogs as $ctlg): ?>

                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="catalog_block">
                            <a href="<?= Url::to(['catalog/cat', 'slug' => $ctlg->slug]) ?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?= Html::img($ctlg->thumb(160, 160), ['class' => 'img-responsive']); ?>
                                    </div>
                                    <div class="col-md-12 klei_info">
                                        <div class="klei_name">
                                            <p><?= $ctlg->title; ?></p>
                                        </div>
                                        <div class="klei_desc">
                                            <?= $ctlg->description; ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <hr>
                                        <?= Html::a('Каталог', ['catalog/cat', 'slug' => $ctlg->slug], ['class' => 'btn']) ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php
            /*echo '<pre>';
            foreach ($catalogs as $ctlg){
                echo '<strong>' . $ctlg->title . '</strong><br/>';
                echo $ctlg->description . '<br/><br/>';
            }
            echo '</pre>';*/
            ?>
        </div>
    </div><!--/container-->
</section>