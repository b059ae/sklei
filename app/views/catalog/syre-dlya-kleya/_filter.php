<?php
/** @var  $items \yii\easyii\modules\catalog\api\ItemObject[] */
/** @var  $cat \yii\easyii\modules\catalog\api\CategoryObject */
/** @var  $filterForm \app\models\SyryeFilterForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<p>Выберите параметры</p>
<div class="well well-sm filter_syre">
<?php $form = ActiveForm::begin(['method' => 'get', 'action' => Url::to(['/catalog/cat', 'slug' => $cat->slug])]); ?>
<?= $form->field($filterForm, 'type')->dropDownList($cat->fieldOptions('type', 'Не выбрано'),['onchange'=>'this.form.submit()']) ?>
<?php ActiveForm::end(); ?>
</div>