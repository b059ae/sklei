<?php

/** @var $item \yii\easyii\modules\catalog\api\ItemObject */
use yii\easyii\modules\feedback\api\Feedback;

?>

    <p>
        <?php if (!empty($item->data->color)) : ?>
            <span class="text-muted">Цвет:</span> <?= implode(', ', $item->data->color) ?>
        <?php endif; ?>
    </p>
    <p>
        <?php if (!empty($item->data->package)) : ?>
            <span class="text-muted">Фасовка:</span> <?= implode(', ', $item->data->package) ?>
        <?php endif; ?>
    </p>
<?php /*
    <p>
        <span class="text-muted">Вид сырья:</span>
        <ul>
            <?php foreach ($item->data->properties as $properties): ?>
                <li><?= $properties ?></li>
            <?php endforeach; ?>
        </ul>
    </p> */ ?>
<?= $item->description ?>
