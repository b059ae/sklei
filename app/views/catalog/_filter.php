<?php
/** @var  $items \yii\easyii\modules\catalog\api\ItemObject[] */
/** @var  $filterForm \app\models\FilterForm */

use app\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>

<p>Выберите параметры</p>
<div class="well well-sm">
<?php $form = ActiveForm::begin([
        'method' => 'get',
        'action' => Url::to(['/catalog/index']),
        'options'=>[
            'class' => 'filter-form',
        ]
]); ?>
<?php /*= $form->field($filterForm, 'properties')->dropDownList($filterForm->getPropertiesOptions(),['onchange'=>'this.form.submit()'])*/ ?>
<?php /*foreach ($filterForm->getPropertiesOptions('') as $value => $label): ?>
    <?=$form->field($filterForm, 'properties[]')->checkbox([
            'value' => $value,
            'label' => Html::ucfirst($label),
    ]); ?>
<?php endforeach;*/ ?>
<?= $form->field($filterForm, 'properties')->checkboxList($filterForm->getPropertiesOptions(''),['itemOptions'=>['onclick'=>'this.form.submit()']]) ?>

<?= $form->field($filterForm, 'color')->dropDownList($filterForm->getColorOptions(),['onchange'=>'this.form.submit()']) ?>
<?= $form->field($filterForm, 'package')->dropDownList($filterForm->getPackageOptions(),['onchange'=>'this.form.submit()']) ?>
<?php ActiveForm::end(); ?>
</div>