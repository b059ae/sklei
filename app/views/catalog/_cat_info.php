<?php
/** @var $this \yii\web\View */
use app\helpers\Html;
use yii\easyii\helpers\Image;
use yii\helpers\Url;
?>

<div class="pr_card_menu col-md-12 col-sm-6 hidden-xs">
    <p>Сертификат соответствия</p>
    <p>
        <a href="<?=Url::to('/uploads/catalog/cert.pdf')?>" target="_blank">
            <?php
            $thumb = Image::thumb('/uploads/catalog/cert.png', 200, null);
            $thumb = '/uploads/catalog/cert.png';
            echo Html::img($thumb, ['class'=>'img-responsive']);//center-block
            ?>
        </a>
    </p>
</div>
<div class="pr_card_menu col-md-12 hidden-sm hidden-xs">
    <p>Не хотите долго искать?</p>
    <button class="btn" data-toggle="modal" data-target=".callback-modal" onclick="metrikaReachGoal('callback-modal')">Свяжитесь с нами</button>
</div>