<?php
/** @var $article \yii\easyii\modules\article\api\ArticleObject */

use yii\easyii\modules\article\api\Article;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $article->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $article->seo('keywords')
]);
$this->title = $article->seo('title', $article->title);
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['articles/index']];
$this->params['breadcrumbs'][] = ['label' => $article->cat->title, 'url' => ['articles/cat', 'slug' => $article->cat->slug]];
$this->params['breadcrumbs'][] = $article->title;
?>

<section class="page">
    <div class="container">
        <div class="header col-md-12 col-sm-12">
            <h1><?= $article->seo('h1', $article->title) ?></h1>
        </div>
        <div class="blog-img col-md-12">
            <?= Html::img($article->thumb(340, 260)) ?>
        </div>
        <div class="blog-page">
            <div>
            <?php if(count($article->text)) : ?>
                        <?= $article->text ?>
            <?php endif; ?>
            </div>
            <div>
            <?php if(count($article->photos)) : ?>
                <div>
                    <h4>Фото</h4>
                    <?php foreach($article->photos as $photo) : ?>
                        <?= $photo->box(100, 100) ?>
                    <?php endforeach;?>
                    <?php Article::plugin() ?>
                </div>
                <br/>
            <?php endif; ?>
            </div>
            <p>
                <?php foreach($article->tags as $tag) : ?>
                    <a href="<?= Url::to(['/articles/cat', 'slug' => $article->cat->slug, 'tag' => $tag]) ?>" class="label label-info"><?= $tag ?></a>
                <?php endforeach; ?>
            </p>

            <p><small class="text-muted"><strong>Дата публикации:</strong> <?=\Yii::$app->formatter->asDate($article->time,'long')?></small></p>
            <p><small class="text-muted"><strong>Просмотры:</strong> <?= $article->views?></small></p>
        </div>
    </div><!--/container-->
</section>