<?php
/** @var $tags array */
/** @var $cat \yii\easyii\modules\article\api\CategoryObject */

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="category-article">
    <?php foreach($tags as $tag) : ?>
    <a href="<?= Url::to(['/articles/cat', 'slug' => $cat->slug, 'tag' => $tag['name']]) ?>">
        <h3><?= $tag['name'] ?> <span><?= $tag['frequency'] ?></span></h3>
    </a>
    <?php endforeach; ?>
</div>
