<?php
/** @var $cat \yii\easyii\modules\article\api\CategoryObject */
/** @var $items \yii\easyii\modules\article\api\ArticleObject[] */
/** @var $tags \yii\easyii\models\Tag[] */

use yii\easyii\modules\article\api\Article;

/** @var $tags array */

$this->registerMetaTag([
    'name' => 'description',
    'content' => $cat->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $cat->seo('keywords')
]);
$this->title = $cat->seo('title', $cat->title);
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['articles/index']];
$this->params['breadcrumbs'][] = $cat->title;
?>

<section class="page blog">
    <div class="container">
        <div class="header col-md-12 col-sm-12 col-xs-12">
            <h1><?= $cat->seo('h1', $cat->title) ?></h1>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
        <?php
        foreach($items as $article) {
            echo $this->render('//articles/_item', ['item' => $article, 'cat' => $cat]);
        }
        ?>
        <!-- Пагинация-->
        <?= $cat->getPages()?>
        <!-- / Пагинация-->
        </div>
    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col-xs-12">
        <div class="category-articles col-md-12 col-sm-6 col-xs-12">
            <h2>Категории:</h2>
            <div class="category-article">
                <?=$this->render('//articles/_tags', ['tags' => $tags, 'cat' => $cat]);?>
            </div>
        </div>
    </div>
    </div><!--/container-->
</section>