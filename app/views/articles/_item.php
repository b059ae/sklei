<?php
/** @var $item \yii\easyii\modules\article\api\ArticleObject */
/** @var $cat \yii\easyii\modules\article\api\CategoryObject */
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="blog-block col-md-6 col-sm-12 col-xs-12">
    <a href="<?= \yii\helpers\Url::to(['articles/view', 'category' => $cat->slug, 'slug' => $item->slug]) ?>">
        <div class="text-center">
            <?= Html::img($item->thumb(340, 260)) ?>
        </div>
        <p class="date"><?= \Yii::$app->formatter->asDate($item->time, 'long') ?></p>
        <h3><?= $item->title ?></h3>
        <p class="desc"><?= $item->short ?></p>
    </a>
    <p class="tags">
        <?php foreach ($item->tags as $tag) : ?>
            <a href="<?= Url::to(['/articles/cat', 'slug' => $item->cat->slug, 'tag' => $tag]) ?>"
               class="label label-info"><?= $tag ?></a>
        <?php endforeach; ?>
    </p>
    <p class="blog_btn"><?= Html::a('Подробнее', ['articles/view', 'category' => $cat->slug, 'slug' => $item->slug], ['class' => 'btn']) ?></p>
</div>
