<?php
/** @var $page \yii\easyii\modules\page\api\PageObject */

/** @var $features \yii\easyii\modules\entity\api\CategoryObject */
/** @var $address \yii\easyii\modules\entity\api\CategoryObject */

/** @var $kley_items \yii\easyii\modules\catalog\api\ItemObject[] */
/** @var  $kley \yii\easyii\modules\catalog\api\CategoryObject */

/** @var  $syrie_items \yii\easyii\modules\catalog\api\ItemObject[] */
/** @var  $syrie \yii\easyii\modules\catalog\api\CategoryObject */

/** @var  $blog \yii\easyii\modules\article\api\CategoryObject */
/** @var  $tags array */

/** @var $tk \yii\easyii\modules\entity\api\ItemObject */

use app\helpers\Html;
use yii\easyii\helpers\Image;
use yii\easyii\models\Setting;
use yii\helpers\Url;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $page->seo('keywords')
]);
$this->title = $page->seo('title');
?>
<section class="first">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h1><?=$page->seo('h1');?></h1>
                <ul>
                    <li><span>Расход 40 грамм на 1 м<sup>2</sup></span></li>
                    <li><span>Без запаха</span></li>
                    <li><span>Простое нанесение</span></li>
                </ul>
                <a href="<?=Url::to(['/catalog'])?>" class="btn" onclick="metrikaReachGoal('main-button-modal'); return true;">Перейти в каталог</a>
            </div>
        </div>
        <?php /*else: ?>
            <div class="row">
                <div class="col-md-12 text-center ">
                    <h1><?=$page->seo('h1');?></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-push-8 col-sm-12 btn-col">
                    <button class="btn" data-toggle="modal" data-target=".main-button-modal" onclick="metrikaReachGoal('main-button-modal')">Заказать<br/>консультацию</button>
                </div>
                <div class="col-md-8 col-md-pull-4 col-sm-12">
                    <ul>
                        <li><span>Цены от 130 за кг.</span></li>
                        <li><span>Товар всегда в наличии. Возможность забрать в день обращения</span></li>
                        <li><span>Доставка по всей России, по Москве и области бесплатно от 100 кг.</span></li>
                        <li><span>Собственная лабаратория позволяет вывести подходящую для Вас формулу</span></li>
                        <li><span>Наши специалисты проведут консультацию и оптимизируют расход клея, а также предоставят образцы</span></li>
                    </ul>
                </div>
            </div>
        <?php endif; */ ?>
<?php /*
        <div class="text col-md-8 col-sm-12">
            <h1><?=$page->seo('h1');?></h1>
        </div>
        <button class="callback-btn btn btn-success col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1" data-toggle="modal" data-target=".callback-modal" onclick="metrikaReachGoal('callback-modal')">Заказать звонок</button>
        <div class="callback col-md-3 col-sm-12">
            <?php //TODO: Убрать h3?>
            <div class="callback-h">Заказать звонок</div>
            <hr>
            <p>
                Оставьте свою заявку, и мы ответим на неё в ближайшее время!
            </p>
            <hr/>
            <?= \app\widgets\CallbackForm::widget() ?>
        </div>
 */ ?>
    </div><!--/container-->
</section>
<section class="second">
    <div class="container">
        <div class="header col-md-12 col-sm-12">
            <h2>О нас</h2>
        </div>
        <div class="onas col-md-5 col-md-offset-1 col-sm-12">
            <h3>Чем занимается наша компания?</h3>
            <p>
                Компания «С-Клей» является поставщиком клея для производства мягкой мебели и матрасов, а также химических компонентов для производства клея. Основное назначение нашего клея - склеивание поролонов между собой, в сочетании с текстилем, деревянными и металлическими поверхностями.
            </p>
        </div>
        <div class="col-md-5 col-md-offset-1 col-sm-12">
            <h3>Мы готовы предложить вам:</h3>
            <ul>
                <?php foreach ($features->getItems() as $feature): ?>
                    <li>
                            <span>
                                <?= $feature->title ?>
                            </span>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div><!--/container-->
</section>

<section class="third">
    <div class="container">
        <div class="header col-md-12 col-sm-12">
            <h2>Лидеры продаж</h2>
            <div class="col-md-8 col-md-offset-2 col-xs-12">
                <p>Мы готовы предложить вам широкий ассортимент клея и химических элементов, использующихся для его производства</p>
            </div>
        </div>
    </div>
    <div class="container">
        <?= \yii\bootstrap\Carousel::widget([
            'options' => ['class' => 'slide', 'id' => 'klei-carousel'],
            /* 'clientOptions' => $this->clientOptions,*/
            'controls' => false,
            'showIndicators' => true,
            'items' => array_map(function ($item) use ($kley) {
                return $this->render('//catalog/klei/_item_carousel', ['item' => $item, 'cat' => $kley]);
            }, $kley_items),
        ]);
        ?>
    </div><!--/container-->
</section>

<section class="fourth">
    <div class="col-md-12">
        <h2>Закажите пробный каталог прямо сейчас!</h2>
    </div>
    <div class="col-md-12 col-sm-12 flex">
        <?= \app\widgets\CatalogForm::widget() ?>
    </div>
</section>
<section class="fifth">
    <div class="container">
        <div class="header col-md-12 col-sm-12">
            <h2>Виды сырья</h2>
        </div>
    </div>
    <div class="container">
        <?= \yii\bootstrap\Carousel::widget([
            'options' => ['class' => 'slide', 'id' => 'syrie-carousel'],
            /* 'clientOptions' => $this->clientOptions,*/
            'controls' => false,
            'showIndicators' => true,
            'items' => array_map(function ($item) use ($syrie) {
                return $this->render('//catalog/syre-dlya-kleya/_item_carousel', ['item' => $item, 'cat' => $syrie]);
            }, $syrie_items),
        ]);
        ?>
    </div><!--/container-->
</section>
<section class="blog">
    <div class="container">
        <div class="header col-md-12 col-sm-12 col-xs-12">
            <h2>Статьи и материалы</h2>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
            <?php
                foreach($blog->getItems(['pagination' => ['pageSize' => 4]]) as $article) {
                    echo $this->render('//articles/_item', ['item' => $article, 'cat' => $blog]);
                }

            ?>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col-xs-12">
            <div class="category-articles col-md-12 col-sm-6 col-xs-12">
                <h2>Категории:</h2>
                <div class="category-article">
                    <?=$this->render('//articles/_tags', ['tags' => $tags, 'cat' => $blog]);?>
                </div>
            </div>
        </div>
    </div><!--/container-->
</section>
<!--ДОСТАВКА-->
<section class="page" id="dostavka">
    <div class="container">
        <div class="delivery header col-md-12 col-sm-12">
            <h2>Доставка</h2>
        </div>
        <div class="delivery header col-md-12 col-sm-12">
            <div class="row">
                <?php foreach ($tk as $tk_item): ?>
                    <div class="delivery_logo col-md-3 col-xs-6">
                        <?php
                        $thumb = Image::thumb($tk_item->logo, 186, null);
                        echo Html::a(Html::img($thumb), $tk_item->url);
                        ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="delivery col-md-12">
            <hr>
            <div class="col-md-6 col-sm-12">
                <p>Дорогие покупатели, мы осуществляем доставку по всей России.</p>
                <p>В отдаленные регионы мы отправляем транспортными компаниями. Сроки, условия, цены доставки рассчитываются по тарифам транспортных компаний (калькулятор доставки есть на каждом сайте транспортной кампании). Если вы задумались о доставке - позвоните нам, наш менеджер поможет выбрать наиболее выгодную доставку или транспортную компанию, которая есть в Вашем городе и, конечно, самый подходящий для Вас товар. Звоните!</p>
                <p>При желании вы можете самостоятельно забрать приобретенные товары с нашего склада. Для этого, оформляя заказ, сообщите менеджеру, что хотите воспользоваться услугой самовывоза</p>
            </div>
            <div class="gazel col-md-6 col-sm-12">
                <?php
                $thumb = Image::thumb('page/gazel.png', 450, null);
                echo Html::img($thumb);
                ?>
            </div>
        </div>
    </div><!--/container-->
</section>
<!--КОНТАКТЫ-->
<section class="contact">
    <div class="container">
        <div class="header col-md-12 col-sm-12">
            <h2>Контактная информация</h2>
        </div>
        <div class="col-md-4 col-sm-12">
            <p><strong>Принимаем заявки по телефону</strong></p>
            <p class="phone"><?= Html::phone(Setting::get('phone')); ?></p>
            <p><strong>E-mail:</strong> <?= Html::mailto(Setting::get('email')) ?></p>
            <p><strong>Адрес производства:</strong><br> <?= $address->getItems()[0]->{'address-production'} ?></p>
            <p><strong>Офис:</strong><br> <?= $address->getItems()[0]->{'address-office'} ?></p>

        </div>
        <div class="col-md-4 col-sm-12">
            <p><strong>Неудобно звонить? Напишите нам:</strong></p>
            <?= \app\widgets\FeedbackForm::widget(); ?>
        </div>
        <div class="col-md-4 col-sm-12">
            <?php
            $parts = explode("#", $address->getItems()[0]->map);
            if (count($parts) == 4): ?>
                <iframe width="100%" height="300" frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/place?q=place_id:<?= $parts[1] ?>&key=<?= Setting::get('gm_api_key') ?>&language=<?= Yii::$app->language ?>"
                ></iframe>
            <?php endif; ?>
        </div>
    </div><!--/container-->
</section>