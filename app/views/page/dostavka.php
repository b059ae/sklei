<?php
/** @var $item \yii\easyii\modules\page\api\PageObject */
/** @var $tk \yii\easyii\modules\entity\api\ItemObject */
/** @var $address \yii\easyii\modules\entity\api\CategoryObject */

use \yii\easyii\helpers\Image;
use yii\easyii\models\Setting;
use app\helpers\Html;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $item->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $item->seo('keywords')
]);
$this->title = $item->seo('title', $item->title);
$this->params['breadcrumbs'][] = $item->title;
?>

<section class="page">
    <div class="container">
        <div class="delivery header col-md-12 col-sm-12">
            <h1><?= $item->seo('h1', $item->title) ?></h1>
            <div class="row">
                <div class="col-md-8 col-xs-12 text-justify">
                    <?= $item->text ?>
                </div>
            </div>
        </div>
        <div class="delivery header col-md-12 col-sm-12">
            <div class="row">
                <?php foreach ($tk as $tk_item): ?>
                    <div class="delivery_logo col-md-3 col-xs-6">
                        <?php
                        $thumb = Image::thumb($tk_item->logo, 186, null);
                        echo Html::a(Html::img($thumb), $tk_item->url);
                        ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="delivery col-md-12">
            <hr>
            <div class="col-md-6 col-sm-12">
                <p><?= $item->{'extra-text'} ?></p>
                <p><strong>Адрес нашего склада:</strong></p>
                <p><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span><?= $address->getItems()[0]->{'address-production'} ?></p>
                <!--<p><span class="glyphicon glyphicon-time" aria-hidden="true"></span> Время работы склада: ПН-ПТ, с 10:00
                    до 19:00</p>-->
                <p><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> Телефон <span class="blue"><?= Html::phone(Setting::get('phone')); ?></span></p>

            </div>
            <div class="gazel col-md-6 col-sm-12">
                <?php
                $thumb = Image::thumb('page/gazel.png', 450, null);
                echo Html::img($thumb);
                ?>
            </div>
        </div>
    </div><!--/container-->
</section>