<?php
/** @var $item \yii\easyii\modules\page\api\PageObject */

/** @var $address \yii\easyii\modules\entity\api\CategoryObject */

use yii\easyii\models\Setting;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $item->seo('description')
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $item->seo('keywords')
]);

$this->title = $item->seo('title', $item->title);
$this->params['breadcrumbs'][] = $item->title;
?>

<section class="page">
    <div class="container">
        <div class="header col-md-12 col-sm-12 col-xs-12">
            <h1><?= $item->seo('h1', $item->title) ?></h1>
        </div>
        <div class="contact_page col-md-6 col-xs-12">
            <?= $item->text ?>
        </div>
        <div class="col-md-6 col-xs-12">
            <?php
            $parts = explode("#", $address->getItems()[0]->map);
            if (count($parts) == 4): ?>
                <iframe width="100%" height="480" frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/place?q=place_id:<?= $parts[1] ?>&key=<?= Setting::get('gm_api_key') ?>&language=<?= Yii::$app->language ?>"
                ></iframe>
            <?php endif; ?>
        </div>
    </div><!--/container-->
</section>