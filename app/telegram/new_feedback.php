<?php
use yii\helpers\Html;
?>
Пользователь <b><?= $feedback->name ?> <?= $feedback->phone ?></b> оставил заявку на сайте. Просмотреть её вы можете <?= Html::a('здесь', $link) ?>.