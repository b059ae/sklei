<?php
/**
 * Created by PhpStorm.
 * User: b059a
 * Date: 10.03.2017
 * Time: 23:11
 */

namespace app\models;


use yii\base\Object;

class Catalog extends Object
{
    /**
     * Название
     * @var string
     */
    public $title;
    /**
     * Категория
     * @var \yii\easyii\modules\catalog\api\CategoryObject
     */
    public $cat;

    /**
     * Товары
     * @var \yii\easyii\modules\catalog\api\ItemObject[]
     */
    public $items;
}