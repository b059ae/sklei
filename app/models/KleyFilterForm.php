<?php
namespace app\models;

use Yii;
use yii\base\Model;

class KleyFilterForm extends Model
{
    public $color;
    public $package;
    public $properties;


    public function rules()
    {
        return [
            [['color','package','properties'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'color' => 'Цвет',
            'package' => 'Упаковка',
            'properties' => 'Свойства',
        ];
    }

    public function parse()
    {
        $filters = [];

        if ($this->color) {
            $filters['color'] = $this->color;
        }
        if ($this->package) {
            $filters['package'] = $this->package;
        }
        if ($this->properties) {
            $filters['properties'] = $this->properties;
        }

        return $filters;
    }
}