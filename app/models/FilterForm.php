<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\easyii\modules\catalog\api\Catalog;

class FilterForm extends Model
{
    public $color;
    public $package;
    public $properties;

    protected $colorOptions = [];
    protected $packageOptions = [];
    protected $propertiesOptions = [];

    public function rules()
    {
        return [
            ['properties', 'each', 'rule' => ['string']],
            [['color','package'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'color' => 'Цвет',
            'package' => 'Упаковка',
            'properties' => 'Свойства',
        ];
    }

    public function parse()
    {
        $filters = [];

        if ($this->color) {
            $filters['color'] = $this->color;
        }
        if ($this->package) {
            $filters['package'] = $this->package;
        }
        if ($this->properties) {
            $filters['properties'] = $this->properties;
        }

        return $filters;
    }

    public function setAllOptions(){
        //Клей
        $cat = Catalog::cat('klei');
        $properties = $cat->fieldOptions('properties');
        $this->setPropertiesOptions($properties);
        $this->setColorOptions($cat->fieldOptions('color'));
        $this->setPackageOptions($cat->fieldOptions('package'));

        //Сырье для клея
        $cat = Catalog::cat('syre-dlya-kleya');
        $properties = $cat->fieldOptions('properties');
        $this->setPropertiesOptions($properties);
        $this->setColorOptions($cat->fieldOptions('color'));
        $this->setPackageOptions($cat->fieldOptions('package'));

        //Оборудование
        $cat = Catalog::cat('oborudovanie');
        $properties = $cat->fieldOptions('properties');
        $this->setPropertiesOptions($properties);
    }


    public function setColorOptions($options){
        $this->setOptions('colorOptions', $options);
    }

    public function getColorOptions($firstOption = 'Не выбрано'){
        return $this->getOptions('colorOptions', $firstOption);
    }

    public function setPackageOptions($options){
        $this->setOptions('packageOptions', $options);
    }

    public function getPackageOptions($firstOption = 'Не выбрано'){
        return $this->getOptions('packageOptions', $firstOption);
    }

    public function setPropertiesOptions($options){
        $this->setOptions('propertiesOptions', $options);
    }

    public function getPropertiesOptions($firstOption = 'Не выбрано'){
        return $this->getOptions('propertiesOptions', $firstOption);
    }

    protected function setOptions($property, $options){
        $this->$property = ArrayHelper::merge($this->$property, $options);
    }

    protected function getOptions($property, $firstOption = ''){
        $options = [];
        if($firstOption) {
            $options[''] = $firstOption;
        }

        return ArrayHelper::merge($options, $this->$property);
    }
}