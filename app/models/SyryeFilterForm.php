<?php
namespace app\models;

use Yii;
use yii\base\Model;

class SyryeFilterForm extends Model
{
    public $color;
    public $package;
    public $type;


    public function rules()
    {
        return [
            [['color','package','type'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'color' => 'Цвет',
            'package' => 'Фасовка',
            'type' => 'Вид сырья',
        ];
    }

    public function parse()
    {
        $filters = [];

        if ($this->color) {
            $filters['color'] = $this->color;
        }
        if ($this->package) {
            $filters['package'] = $this->package;
        }
        if ($this->type) {
            $filters['type'] = $this->type;
        }

        return $filters;
    }
}