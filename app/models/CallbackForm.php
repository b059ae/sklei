<?php

namespace app\models;

use app\validators\NameMatchValidator;

class CallbackForm extends \yii\easyii\modules\feedback\models\Feedback
{

    public function formName()
    {
        return 'Feedback';
    }
    
    public function attributeLabels()
    {
        return [
            'name' => 'Введите имя',
            'phone' => 'Введите телефон',
        ];
    }

    public function rules()
    {
        return [
            [['name','phone'], 'required'],
            [['name'], NameMatchValidator::class],
            ['phone', 'filter', 'filter' => function ($v) {
                if (substr($v, 0, 2) == '+7') {
                    $v = str_replace(['+', '(', ')', ' ', '-'], [], substr($v, 2));
                }
                return $v;
            }],
            ['phone', 'match', 'pattern' => '/[0-9]+$/s',
                'message' => 'Номер телефона необходимо вводить в формате +7 (900) 333-2211'],
        ];
    }
}