<?php
namespace app\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * Форма заказа каталог
 * Class CatalogForm
 * @package app\widgets
 */
class CatalogForm extends AbstractAjaxForm
{

    public $thankTitle = 'Спасибо за Вашу заявку!';
    public $thankText = 'Наши специалисты оперативно свяжутся и проконсультируют Вас по видам и характеристикам продукции.';

    public function run()
    {
        $model = new \app\models\CallbackForm();

        $form = ActiveForm::begin([
            'id' => $this->formId,
            'enableClientValidation' => true,
            'action' => Url::to(['/admin/feedback/send']),
            'options' => [
                'class' => 'flex',
            ],
        ]);

        /*echo Html::hiddenInput('errorUrl', Url::current());
        echo Html::hiddenInput('successUrl', Url::current());*/

        echo $form->field($model, 'name', [
            'labelOptions' => [
                'class' => 'sr-only',
            ],
            'options'=>[
                'class'=>'col-md-4 col-sm-12 col-xs-12',
            ]
        ])->textInput([
            'placeholder' => $model->getAttributeLabel('name')
        ]);

        echo $form->field($model, 'phone',[
            'labelOptions' => [
                'class' => 'sr-only',
            ],
            'options'=>[
                'class'=>'col-md-4 col-sm-12 col-xs-12',
            ]
        ])->textInput()->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7 (999) 999 99 99',
            'options' => [
                'autocomplete' => 'off',
                'id' => 'form-catalog-phone',
                'class' => 'form-control',
                'placeholder' => $model->getAttributeLabel('phone')
            ],
        ]);

        /*echo $form->field($model, 'phone', [
            'labelOptions' => [
                'class' => 'sr-only',
            ],
            'options'=>[
                'class'=>'col-md-4 col-sm-12 col-xs-12',
            ]
        ])->textInput([
            'placeholder' => $model->getAttributeLabel('phone')
        ]);*/

        echo "<div class='col-md-4 col-sm-12 col-xs-12'>" . Html::submitButton('Получить каталог', ['class' => 'btn btn-success', 'onclick'=>"metrikaReachGoal('catalog')"]) . "</div>";

        ActiveForm::end();

        $this->registerJs();
    }

}