<?php
namespace app\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * Форма обратного звонка
 * Class CallbackForm
 * @package app\widgets
 */
class CallbackForm extends AbstractAjaxForm
{

    public $button = 'Жду звонка';
    public $thankTitle = 'Спасибо за Вашу заявку!';
    public $thankText = 'Наши специалисты оперативно свяжутся и проконсультируют Вас по видам и характеристикам продукции.';
    public $metrikaGoal = 'callback';

    public function run()
    {
        $model = new \app\models\CallbackForm();

        $form = ActiveForm::begin([
            'id' => $this->formId,
            'enableClientValidation' => true,
            'action' => Url::to(['/site/callback'])
        ]);

        /*echo Html::hiddenInput('errorUrl', Url::current());
        echo Html::hiddenInput('successUrl', Url::current());*/

        echo $form->field($model, 'name')->textInput();

        echo $form->field($model, 'phone',[
        ])->textInput()->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7 (999) 999 99 99',
            'options' => [
                'autocomplete' => 'off',
                'id' => $this->formId.'catalog-phone',
                'class' => 'form-control',
            ],
        ]);

        /*echo $form->field($model, 'phone')->textInput();*/

        echo "<div class='jdu form-group'>" . Html::submitButton($this->button, ['class' => 'btn btn-primary', 'onclick'=>"metrikaReachGoal('".$this->metrikaGoal."')"]) . "</div>";
        ActiveForm::end();

        $this->registerJs();
    }

}