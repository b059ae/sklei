<?php
/** @var $this \yii\web\View */
/** @var $id string */
/** @var $title string */
/** @var $text string */

?>
<div id="<?=$id?>" class="modal fade thank-modal" tabindex="-1" role="dialog" aria-labelledby="thankModal" aria-hidden="true">
    <div class="modal-dialog modal-lg flex">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="callback-h"><?=$title?></div>
            <hr>
            <p><?=$text?></p>
        </div>
    </div>
</div>