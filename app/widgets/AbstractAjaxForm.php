<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\web\View;

/**
 * Форма с отправкой данных аяксом
 * Class AbstractAjaxForm
 * @package app\widgets
 */
abstract class AbstractAjaxForm extends Widget
{


    /**
     * ID Формы
     * @var string
     */
    protected $formId;
    /**
     * ID модального окна с благодарностью
     * @var string
     */
    protected $thankModalId;
    /**
     * Заголовок модального окна с благодарностью
     * @var string
     */
    public $thankTitle;
    /**
     * Текст модального окна с благодарностью
     * @var string
     */
    public $thankText;

    public function init()
    {
        $this->formId = $this->id . '-form';
        $this->thankModalId = $this->id . '-thank-modal';
        parent::init();
    }

    /**
     * Регистрация аякс-обработчика
     */
    public function registerJs()
    {
        $script = <<< JS
                    $('body').on('beforeSubmit', 'form#{$this->formId}', function () {
                         var form = $(this);
                         // return false if form still have some validation errors
                         if (form.find('.has-error').length) {
                              return false;
                         }
                         // submit form
                         $.ajax({
                              url: form.attr('action'),
                              type: 'post',
                              data: form.serialize(),
                              beforeSend: function () {
                                   //hide all modals
                                   $('.modal').modal('hide');
                                   //remove values in fields
                                   form.find('input[type="text"]').val('');
                                   form.find('textarea').val('');
                                   //thank you modal show
                                   $('#{$this->thankModalId}').modal('show');
                              }
                         });
                         return false;
                    });
JS;
        $this->view->registerJs($script, View::POS_READY);

        Yii::$app->view->params['modals'][] = $this->thankModal();
    }

    /**
     * Модальное окно с благодарностью
     * @return string
     */
    public function thankModal()
    {
        return $this->render('_thank_modal', [
            'id' => $this->thankModalId,
            'title' => $this->thankTitle,
            'text' => $this->thankText,
        ]);
    }
}