<?php
namespace app\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * Форма обратной связи
 * Class FeedbackForm
 * @package app\widgets
 */
class FeedbackForm extends AbstractAjaxForm
{

    public $thankTitle = 'Спасибо за Ваш вопрос!';
    public $thankText = 'Наши специалисты оперативно свяжутся и проконсультируют Вас.';

    public function run()
    {
        $model = new \app\models\FeedbackForm();
        $form = ActiveForm::begin([
            'id' => $this->formId,
            'enableClientValidation' => true,
            'action' => Url::to(['/site/feedback'])
        ]);

        /*        echo Html::hiddenInput('errorUrl', Url::current());
                echo Html::hiddenInput('successUrl', Url::current());*/

        echo $form->field($model, 'name', [
            'labelOptions' => [
                'class' => 'sr-only',
            ],
        ])->textInput([
            'placeholder' => $model->getAttributeLabel('name')
        ]);

        echo $form->field($model, 'phone',[
            'labelOptions' => [
                'class' => 'sr-only',
            ],
        ])->textInput()->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+7 (999) 999 99 99',
            'options' => [
                'autocomplete' => 'off',
                'id' => 'form-feedback-phone',
                'class' => 'form-control',
                'placeholder' => $model->getAttributeLabel('phone')
            ],
        ]);

        /*echo $form->field($model, 'phone', [
            'labelOptions' => [
                'class' => 'sr-only',
            ],
        ])->textInput([
            'placeholder' => $model->getAttributeLabel('phone')
        ]);*/

        echo $form->field($model, 'text', [
            'labelOptions' => [
                'class' => 'sr-only',
            ],
        ])->textarea([
            'placeholder' => $model->getAttributeLabel('text'),
        ]);

        echo Html::submitButton('Отправить сообщение', ['class' => 'btn btn-primary', 'onclick'=>"metrikaReachGoal('feedback')"]);
        ActiveForm::end();

        $this->registerJs();
    }

}