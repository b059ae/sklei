<?php
/**
 * Генератор карты сайта
 */

use himiklab\sitemap\behaviors\SitemapBehavior;
use yii\helpers\Url;

return [
    'class' => 'himiklab\sitemap\Sitemap',
    'models' => [
        [
            // Статьи блога
            'class' => yii\easyii\modules\article\models\Item::class,
            'behaviors' => [
                'sitemap' => [
                    'class' => SitemapBehavior::className(),
                    'scope' => function ($model) {
                        /** @var \yii\db\ActiveQuery $model */
                        $model
                            ->select(['slug', 'time'])
                            ->andWhere(['category_id' => 1])// Новости
                            ->andWhere(['status' => yii\easyii\modules\article\models\Item::STATUS_ON]);// Только опубликованные
                    },
                    'dataClosure' => function ($model) {

                        /** @var yii\easyii\modules\article\models\Item $model */
                        return [
                            'loc' => Url::to('blog/' . $model->slug, true),
                            'lastmod' => $model->time,
                            'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                            'priority' => 0.8
                        ];
                    }
                ],
            ],
        ],
        [
            // Каталог
            'class' => yii\easyii\modules\catalog\models\Item::class,
            'behaviors' => [
                'sitemap' => [
                    'class' => SitemapBehavior::className(),
                    'scope' => function ($model) {
                        $itemsTableName = \yii\easyii\modules\catalog\models\Item::tableName();
                        $catTableName = \yii\easyii\modules\catalog\models\Category::tableName();
                        /** @var \yii\db\ActiveQuery $model */
                        $model
                            ->select([$itemsTableName.'.slug', 'time', 'category_id'])
                            ->join('INNER JOIN', $catTableName, $itemsTableName.'.category_id = ' . $catTableName . '.id')
                            ->andWhere([
                                $itemsTableName.'.status' => yii\easyii\modules\catalog\models\Item::STATUS_ON,
                                $catTableName.'.status' => yii\easyii\modules\catalog\models\Category::STATUS_ON,
                            ]);// Только опубликованные
                    },
                    'dataClosure' => function ($model) {
                        /** @var yii\easyii\modules\article\models\Item $model */
                        return [
                            'loc' => Url::to($model->category->slug.'/' . $model->slug, true),
                            'lastmod' => $model->time,
                            'changefreq' => SitemapBehavior::CHANGEFREQ_DAILY,
                            'priority' => 0.8
                        ];
                    }
                ],
            ],
        ],
    ],
    'urls' => array_map(function ($item) {
        //Параметры по умолчанию для changefreq и priority
        $item['changefreq'] = isset($item['changefreq'])
            ? $item['changefreq']
            : SitemapBehavior::CHANGEFREQ_DAILY;
        $item['priority'] = isset($item['priority'])
            ? $item['priority']
            : 0.8;
        return $item;
    }, [
        // Главная страница
        ['loc' => '/',],
        // Блог
        ['loc' => 'blog',],
        // Контактная информация
        ['loc' => 'kontaktnaya-informaciya',],
        // Каталог
        ['loc' => 'catalog',],
//                // your additional urls
//                [
//                    'loc' => '/news/index',
//                    'changefreq' => \himiklab\sitemap\behaviors\SitemapBehavior::CHANGEFREQ_DAILY,
//                    'priority' => 0.8,
//                    'news' => [
//                        'publication'   => [
//                            'name'          => 'Example Blog',
//                            'language'      => 'en',
//                        ],
//                        'access'            => 'Subscription',
//                        'genres'            => 'Blog, UserGenerated',
//                        'publication_date'  => 'YYYY-MM-DDThh:mm:ssTZD',
//                        'title'             => 'Example Title',
//                        'keywords'          => 'example, keywords, comma-separated',
//                        'stock_tickers'     => 'NASDAQ:A, NASDAQ:B',
//                    ],
//                    'images' => [
//                        [
//                            'loc'           => 'http://example.com/image.jpg',
//                            'caption'       => 'This is an example of a caption of an image',
//                            'geo_location'  => 'City, State',
//                            'title'         => 'Example image',
//                            'license'       => 'http://example.com/license',
//                        ],
//                    ],
//                ],
    ]),
//            'enableGzip' => true, // default is false
    'cacheExpire' => 1, // 1 second. Default is 24 hours
];