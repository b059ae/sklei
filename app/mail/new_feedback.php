<?php
use yii\helpers\Html;

$this->title = $subject;
?>
<p>Пользователь <b><?= $feedback->name ?> <?= $feedback->phone ?></b> оставил заявку на сайте.</p>
<p>Просмотреть её вы можете <?= Html::a('здесь', $link) ?>.</p>
<hr>
<p>Это автоматическое сообщение и на него не нужно отвечать.</p>
