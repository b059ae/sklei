<?php

namespace app\helpers;

use app\models\CallbackForm;
use app\models\FeedbackForm;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

class Html extends \yii\helpers\Html
{

    /**
     * Форматирование телефона
     * @param string $phone
     * @return string
     */
    public static function phone($phone)
    {
        $arr = explode(' ', $phone);

        return '<a href="tel:+7' . substr(preg_replace('/[^0-9]+/', '', $phone), 1) . '" class="number">' . $arr[0] . ' <strong>' . $arr[1] . '</strong></a>';
    }

    /**
     * Преобразует первый символ строки в верхний регистр
     * @param string $str
     * @return string
     */
    public static function ucfirst($str){
        $fc = mb_strtoupper(mb_substr($str, 0, 1, 'utf-8'), 'utf-8');
        return $fc.mb_substr($str, 1, null, 'utf-8');
    }

    /**
     * Удаление текста до цифр
     * @param $str
     */
    public static function removeFirstLetters($str){
        return strtr(preg_replace('/([\s\S]*?)([0-9]+?)([\s\S]+?)/si', '$2$3', $str),
            [
                ' '=>'&nbsp;'
            ]);
    }
    /**
     * Форматирование переносов строк
     * @param $str
     */
    public static function formatBr($str){
        return strtr($str,
            [
                "\n"=>'<br/>'
            ]);
    }
}