<?php

namespace app\components;

use Yii;
use yii\base\Component;

/**
 * Class ABTest
 * Компонент для AB-тестирования. Основной метод
 * @package app\components
 */
class ABTest extends Component
{
    /**
     * Название куки для хранения
     * @var string
     */
    public $cookieName = 'ab_test';
    /**
     * Допустимые значения для теста
     * @var array
     */
    public $availableValues = ['igor','vladimir'];
    /**
     * Текущее значение теста
     * @var string
     */
    protected $value;
    
    public function init()
    {
        if (!Yii::$app->request->cookies[$this->cookieName]){
            $this->value = rand(0,1) == 1
                ? $this->availableValues[1]
                : $this->availableValues[0];
            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => $this->cookieName,
                'value' =>$this->value,
            ]));
        }else{
            $this->value = Yii::$app->request->cookies[$this->cookieName]->value;
        }
        parent::init();
    }

    /**
     * Возвращает текущее значение тестаа
     * @return string
     */
    public function getValue(){
        return $this->value;
    }
}