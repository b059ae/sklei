User-Agent: Yandex
Disallow: /*utm*
Disallow: /*pz=*
Disallow: */apple-app-site-association$
Disallow: */assetlinks.json$

User-Agent: Googlebot
Disallow: /*utm*
Disallow: /*pz=*
Disallow: */apple-app-site-association$
Disallow: */assetlinks.json$

User-agent: *
Disallow: /*utm*
Disallow: /*pz=*
Disallow: */apple-app-site-association$
Disallow: */assetlinks.json$

Host: s-klei.ru
Sitemap: http://s-klei.ru/sitemap.xml